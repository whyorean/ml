import networkx as nx
import pandas as pd

from matplotlib import pyplot
from pgmpy.estimators import MaximumLikelihoodEstimator
from pgmpy.inference import VariableElimination
from pgmpy.models import BayesianModel

data = pd.read_csv("data7.csv")
heart_disease = pd.DataFrame(data)

model = BayesianModel([
    ('Age', 'Lifestyle'),
    ('Gender', 'Lifestyle'),
    ('Family', 'Heartdisease'),
    ('Diet', 'Cholestrol'),
    ('Lifestyle', 'Diet'),
    ('Cholestrol', 'Heartdisease')
])

model.fit(heart_disease, estimator=MaximumLikelihoodEstimator)
nx.draw(model)
pyplot.savefig('77.png')
HeartDisease_infer = VariableElimination(model)

q = HeartDisease_infer.query(variables=['Heartdisease'], evidence={'Age': 2})
print(q)
