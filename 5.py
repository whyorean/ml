import pandas as pd
from sklearn.metrics import accuracy_score
from sklearn.model_selection import train_test_split
from sklearn.naive_bayes import GaussianNB
from sklearn.preprocessing import LabelEncoder

data = pd.read_csv('data3.csv')
print("Given data set :\n", data)

X = data.iloc[:, :-1]
y = data.iloc[:, -1]

lb = LabelEncoder()
X.Outlook = lb.fit_transform(X.Outlook)
X.Temperature = lb.fit_transform(X.Temperature)
X.Humidity = lb.fit_transform(X.Humidity)
X.Windy = lb.fit_transform(X.Windy)
y = lb.fit_transform(y)

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.20)

classifier = GaussianNB()
classifier.fit(X_train, y_train)

print("Accuracy is:", accuracy_score(classifier.predict(X_test), y_test))
