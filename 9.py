import numpy as np
from sklearn.datasets import load_iris
from sklearn.model_selection import train_test_split
from sklearn.neighbors import KNeighborsClassifier

dataset = load_iris()
X_train, X_test, y_train, y_test = train_test_split(dataset.data, dataset.target)

knc = KNeighborsClassifier(n_neighbors=4)
knc.fit(X_train, y_train)

for i in range(len(X_test)):
    x = X_test[i]
    x_new = np.array([x])
    prediction = knc.predict(x_new)
    print("Actual=", dataset["target_names"][y_test[i]], "Predicted=",dataset["target_names"][prediction[0]])
print(knc.score(X_test, y_test))
