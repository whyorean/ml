def get_s_set():
    file_handler = open("data.csv", "r")
    file_handler.readline()
    first_line = file_handler.readline()
    first_row = first_line.split(sep=' ')
    for line in file_handler.readlines():
        row = line.split(sep=' ')
        if row[-1] == "yes\n":
            for i in range(len(row)):
                if first_row[i] != row[i]:
                    first_row[i] = "?"
    return first_row


def get_g_set(_s_set):
    _s_set.pop(-1)
    _g_set = []
    length = len(_s_set)
    for i in range(length):
        tmp = []
        for j in range(length):
            if _s_set[i] == "?":
                continue
            if j == i:
                tmp.append(_s_set[j])
            else:
                tmp.append("?")
        if len(tmp) > 0:
            _g_set.append(tmp)
    return _g_set


def check_consistency(_sub_set):
    _sub_set.append("yes/n")
    file_handler = open("data.csv", "r")
    file_handler.readline()
    result = True
    for line in file_handler.readlines():
        row_list = line.split(" ")
        length = len(row_list)
        for i in range(length):
            if row_list[i] != "?" \
                    and _sub_set[i] != "?" \
                    and row_list[i] != _sub_set[i]:
                result = False
    return result


s_set = get_s_set()
print("Specific Set ->", s_set)
g_set = get_g_set(s_set)
print("Total General Set ->", g_set)
f_set = []
for sub_set in g_set:
    if check_consistency(sub_set):
        f_set.append(sub_set)
print("Final General Set ->", f_set)
