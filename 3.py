import math
from collections import Counter

import pandas as pd

p_val = "Yes"
n_val = "No"
main_set = pd.read_csv('data3.csv', sep=',')
data_set = main_set


def get_entropy(_pos, _neg):
    if _pos == _neg:
        return _pos, _neg, 1
    elif _pos == 0 or _neg == 0:
        return _pos, _neg, 0
    else:
        pp = _pos / (_pos + _neg)
        nn = _neg / (_pos + _neg)
        return _pos, _neg, round(-pp * math.log2(pp) - nn * math.log2(nn), 3)


def get_avg_entropy(_pos, _neg, t_pos, t_neg, _entropy):
    return round(((_pos + _neg) / (t_pos + t_neg)) * _entropy, 3)


def get_gain(_entropy, _gain):
    return round(_entropy - _gain, 3)


def get_max_gain(dict):
    return max(dict, key=dict.get)


def get_set_entropy():
    cc = Counter(main_set['PlayTennis'])
    _pos = cc.get(p_val)
    _neg = cc.get(n_val)
    return get_entropy(_pos, _neg)


def get_attr_entropy(t_pos, t_neg, t_entropy, _data_set):
    _dict_attr = {}
    _dict_attr_individual_entropy = {}
    _dict_attr_entropy = {}
    _dict_attr_gain = {}
    target = 'PlayTennis'
    for attr in _data_set.head(0):
        cc = Counter(_data_set[attr])
        attr_entropy = 0
        for item in cc.items():
            key = item[0]
            total = item[1]
            _dict_attr[key] = total
            _pos = len(_data_set[(_data_set[attr] == item[0]) & (_data_set[target] == 'Yes')])
            _neg = total - _pos
            pp, nn, _entropy = get_entropy(_pos, _neg)
            _dict_attr_individual_entropy[key] = _entropy
            attr_entropy += get_avg_entropy(pp, nn, t_pos, t_neg, _entropy)
            _dict_attr_entropy[attr] = attr_entropy
            _dict_attr_gain[attr] = get_gain(t_entropy, attr_entropy)
    # print(_dict_attr)
    # print(_dict_attr_individual_entropy)
    # print(_dict_attr_entropy)
    # Remove Target Gain
    _dict_attr_gain.pop(target)
    attr = get_max_gain(_dict_attr_gain)
    return attr, _dict_attr_individual_entropy


pos, neg, entropy = get_set_entropy()
print("Total Entropy", pos, neg, entropy)
max_gain_attr, dict_attr_individual_entropy = get_attr_entropy(pos, neg, entropy, data_set)
print(dict_attr_individual_entropy)
domain_list = list(data_set[max_gain_attr].unique())

for domain in domain_list:
    if dict_attr_individual_entropy.get(domain) == 0:
        print(domain, " is Leaf node")
    else:
        data_set = main_set[(main_set[max_gain_attr] == domain)]
        mm, dd = get_attr_entropy(pos, neg, entropy, data_set)
        print(domain, "->", mm)
